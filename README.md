# iRobotSonny2

This repository contains data and code used in  
  
**A Low-Cost Robotic Oculomotor Simulator for Assessing Eye Tracking Accuracy in Health and Disease**
  
by _Al Lotze\*, Kassia Love\*, Anca Velisar M.Sc.\*, Natela M. Shanidze Ph.D._


## Table of contents

**3Dprint** - contains the .stl files for the 3D printable parts of the alternative 3D-print version of EyeRobot shown in Figure3 and the eyeballs used in both versions.  

**Arduino scripts** - script necessary to control the motors of the EyeRobot and a tutorial text file

**data** - data used to obtain the results presented in the paper

**Python scripts** - Python script that extracts the reference data from messagepack file outputted by Pupil Core software and saves it in .csv file format 


### Special note about the data

For broad access, we uploaded the eye tracker data as .csv files. The Pupil Player-readable video eyetracking data files are available by request.
  
The raw eyetracking data was preprocessed in Pupil Player v3.2.20 part of the Pupil Core software. 
  
#### data folder
    * Binocular Recordings: eyetracking data related with the binocular recordings made with PupilLabs eyetracker on EyeRobot using Pupil Core software
                            
  
                            In PupilPlayer, were added :
                                  * Annotations related to start and end of the tasks 
                                  * Manual or automatic detections of markers, grid positions or laser spot positions named by the Pupil Core software as references
                                  * Pupil positions were exported to .csv file after offline redetection and fixing the 3D eye model. 
  
                            Using Python script read_refrences.py, references data was converted to .csv file for easy access  
  
        - star15sacc: eyetracking data of simulated 15 degrees saccades movements performed by the EyeRobot
            + annotations.csv 
            + pupil_positions.csv
            + refernce_locations1.csv: first, the laser spot positions are recorded for start and end of each saccade; following from frame 13363 by the grid markers locations
                                       data was used for results presented in Table 2a and Figure 8a,b 
                                       conversion from pixels to degrees was made by calculating the pixels per degree coefficient. The averge of several outer grid marker locations  
                                       relative to the location of the central marker was divided by the respective visual angle (15deg).
  
        - star3sacc: eyetracking data of simulated 3 degrees saccades movements performed by the EyeRobot
            + annotations.csv
            + pupil_positions.csv:    data was used for results presented in Figure 10c
            + refernce_locations.csv: first, the laser spot positions are recorded for start and end of each saccade; following from frame 3556 by the grid markers locations
                                      data was used for results presented in Table 2b
                                      the pixels per degree coefficient was recalculated using similar method as above and used to convert to degrees
  
        - ramps: eyetracking data of simulated binocular smooth pursuit movements performed by the EyeRobot
            + annotations.csv
            + pupil_positions.csv:    data was used for results presented in Figure 10d
  
    * Motor Tests: measurements of the Incremental steps task presented in section **2.3.1**
        - IncrementalSteps.csv:       data was used for results presented in Table 1 and Figure 7

#### video data
- eyetracking monocular video recordings are presented on YouTube channel of The Smith-Kettlewell Eye Research Institute
- the data was used for results presented in Figure 9

links to videos:

[noOffset](https://youtu.be/wEXAGMJizIg)  
[5degRHOffset](https://youtu.be/BdzPaTAX1bM)  
[11RH12UVOffset](https://youtu.be/oBCYYYJSOus)  
  
## Contributors ##
_Anca Velisar_ [@Shanidze Lab](https://www.ski.org/lab/shanidze-lab)
_Natela Shanidze_ [@Shanidze Lab](https://www.ski.org/lab/shanidze-lab)
_Al Lotze_ [@Shanidze Lab](https://www.ski.org/lab/shanidze-lab)

/*
This program is intended to be used with the 'screen' terminal program in OS X
or Linux/Unix. It should also be possible to run it using Putty or a similar 
terminal emulator running on Windows.

To communicate with the Arduino using 'screen':

In a terminal window, type:  ls /dev/tty.*  to get the ID of the serial port.

Then:  screen -L /dev/tty.usbXXXXX 9600

To exit screen, type ctrl/a and then d for "Detach." 
*/
//-------------------------------------------------
//Define constants for the GPIO pins:
const int _LX1stepPin = 5;            //LX motor is motor 1
const int _LX1dirPin = 4;

const int _LY2stepPin = 8;            //LX motor is motor 2
const int _LY2dirPin = 7;

const int _RX3stepPin = 11;           //RX motor is motor 3
const int _RX3dirPin = 12;

const int _RY4stepPin = 3;            //RY motor is motor 4
const int _RY4dirPin = 2;

const bool _leftEye = true;
const bool _rightEye = false;


//the axis struct and axisArray are used for the Move function
struct axis
{
  int numSteps;
  int stepPin;
  float slope;
};
struct axis axisArray[5];


//currentNN keeps track of each axis' position
int currentLX = 0;
int currentLY = 0;
int currentRX = 0;
int currentRY = 0;


//Return values for MakeRadialPoint
int vectorX;
int vectorY;


//targetPointArray holds target coordinates for complex motion sequences. These coordinates are
//loaded into the array by SetTargetPoints, via StuffTargetPoints.
//SetTargetPoints also outputs a list of C assignment statements for the target points to the 
//terminal upon conclusion. These can then be (rather inelegantly) retrieved from the screen log
//file and pasted (hard coded) into the LoadSavedTargets function for later use.
struct targetPoints
{
  int LX;
  int LY;
  int RX;
  int RY;
};
struct targetPoints targetPointArray[25];

int numTargetPoints = 0;    //Number of target points defined by SetTargetPoints


char globalChar;    //used to hold the return value of GetKey
//**************************************************************************************************
void setup() 
{
   int index;
    
    Serial.begin(9600);    
    Serial.setTimeout(600000);

    pinMode(2, OUTPUT);  //direction
    pinMode(3, OUTPUT);  //step
  
    pinMode(4, OUTPUT);  //direction
    pinMode(5, OUTPUT);  //step
  
    pinMode(7, OUTPUT);  //direction
    pinMode(8, OUTPUT);  //step
  
    pinMode(12, OUTPUT);  //direction
    pinMode(11, OUTPUT);  //step


    //Initialize outputs:
    for (index = 1; index <= 6; index++) {digitalWrite(index,LOW);}
    digitalWrite(7,HIGH);     //Due to the geometry of the motor mountings, the left y axis motor 
                              //turns opposite the rest
    for (index = 8; index <= 13; index++) {digitalWrite(index,LOW);}
}
//**************************************************************************************************************
void AssignMotorPins()    //axisArray[1..4] refers to motors 1 through 4; this initializes the array.
{                         //Called by Move.
  axisArray[1].stepPin = _LX1stepPin;
  axisArray[2].stepPin = _LY2stepPin;
  axisArray[3].stepPin = _RX3stepPin;
  axisArray[4].stepPin = _RY4stepPin;
}
//**************************************************************************************************
void ClearScreen()    //Clears terminal screen
{ 
  Serial.write(27); 
  Serial.print("[2J"); // clear screen 
  Serial.write(27); // ESC 
  Serial.print("[H"); // cursor to home 
} 
//**************************************************************************************************
void WaitForReturn()   //Pauses execution until the return key is pressed
{
  char theByte = "";
    while(theByte!=13 && theByte!=10)
    {
      if (Serial.available() > 0) 
      {theByte = Serial.read();}
    }
}
//**************************************************************************************************************
void delayMicrosecondsFlex(long theDelay)     //uses delay or delayMicroseconds as appropriate;
{                                             //delayMicroseconds can't take values over 16383
  if (theDelay <= 16383) {delayMicroseconds(theDelay);} else {delay(theDelay/1000);}
}                                                  
//**************************************************************************************************************
char GetKey()     //Returns a keypress
{
  char inputBuffer[1] = "";
  char theChar;

      Serial.readBytes(inputBuffer,1);
      theChar = inputBuffer[0];

      return theChar;
   
      memset(inputBuffer, 0, sizeof(inputBuffer));
}
//**************************************************************************************************************
void Step(int pinNumber)      //Sends one pulse to the step pin of a driver board. Only used by Position.
{
    digitalWrite(pinNumber,HIGH);
    delay(20);
    digitalWrite(pinNumber,LOW);
    delay(20);
}
//**************************************************************************************************************
void Step10(int pinNumber)       //Sends 10 pulses pulses to the step pin of a driver board. Only used by Position.
{
    int index;
  
    for (index = 1;index <= 10;index++) 
    {
      digitalWrite(pinNumber,HIGH);
      delay(2);
      digitalWrite(pinNumber,LOW);
      delay(2);
    }
}
//**************************************************************************************************************
void Position()                          //Manual positioning using the 1, 2, 3 and 4 keys to step the LX, LY,
{                                        //RX and RY motors respectively. Holding the shift key reverses
   globalChar = '-';                     //direction. The motors move one step per keypress, though depending
                                         //on the host OS keys may auto-repeat if held down.
   Serial.println();                     //The a, s, d and f keys work as above, but in 10 step increments.
   Serial.println();
   Serial.println("1 = left X");
   Serial.println("2 = left Y");
   Serial.println("3 = right X");
   Serial.println("4 = right Y");
   Serial.println("Shift to reverse direction, a, s, d, f for rapid movement");
   Serial.println();
   Serial.println("Type 'e' to exit positioning, or 'z' to exit and set this point to 0,0,0,0");


  while ((globalChar != 'e') && (globalChar != 'z'))
  {
      globalChar=GetKey();

      if (globalChar == '1') {digitalWrite(_LX1dirPin,LOW); Step(_LX1stepPin);currentLX++;}
      if (globalChar == '!') {digitalWrite(_LX1dirPin,HIGH); Step(_LX1stepPin);currentLX--;}
      if (globalChar == '2') {digitalWrite(_LY2dirPin,HIGH); Step(_LY2stepPin);currentLY++;}     
      if (globalChar == '@') {digitalWrite(_LY2dirPin,LOW); Step(_LY2stepPin);currentLY--;}
      if (globalChar == '3') {digitalWrite(_RX3dirPin,LOW); Step(_RX3stepPin);currentRX++;}   
      if (globalChar == '#') {digitalWrite(_RX3dirPin,HIGH); Step(_RX3stepPin);currentRX--;}
      if (globalChar == '4') {digitalWrite(_RY4dirPin,LOW); Step(_RY4stepPin);currentRY++;}   
      if (globalChar == '$') {digitalWrite(_RY4dirPin,HIGH); Step(_RY4stepPin);currentRY--;}

      if (globalChar == 'a') {digitalWrite(_LX1dirPin,LOW); Step10(_LX1stepPin); currentLX+=10;}
      if (globalChar == 'A') {digitalWrite(_LX1dirPin,HIGH); Step10(_LX1stepPin); currentLX-=10; }
      if (globalChar == 's') {digitalWrite(_LY2dirPin,HIGH); Step10(_LY2stepPin); currentLY+=10;}     
      if (globalChar == 'S') {digitalWrite(_LY2dirPin,LOW); Step10(_LY2stepPin); currentLY-=10;}
      if (globalChar == 'd') {digitalWrite(_RX3dirPin,LOW); Step10(_RX3stepPin); currentRX+=10;}   
      if (globalChar == 'D') {digitalWrite(_RX3dirPin,HIGH); Step10(_RX3stepPin);currentRX-=10;}
      if (globalChar == 'f') {digitalWrite(_RY4dirPin,LOW); Step10(_RY4stepPin);currentRY+=10;}   
      if (globalChar == 'F') {digitalWrite(_RY4dirPin,HIGH); Step10(_RY4stepPin);currentRY-=10;}
  }       

  if (globalChar == 'z') {SetToZero();}

}
//**************************************************************************************************
void GetSlopes()    //Called by Move. Gets the slopes of the three non-major axes and stuffs them
{                   //into axisArray
  float floatDivisor = axisArray[1].numSteps;

  axisArray[2].slope = axisArray[2].numSteps/floatDivisor;
  axisArray[3].slope = axisArray[3].numSteps/floatDivisor;
  axisArray[4].slope = axisArray[4].numSteps/floatDivisor;
}
//**************************************************************************************************
void SortAxisArray()    //Sorts axisArray in descending order by number of steps. Called by Move. 
{
  int index;
  bool swapped;
  axis swap;

  do 
  {
     swapped = false;

      for (index = 1;index <= 3;index++)  
      {
       if (axisArray[index].numSteps < axisArray[index+1].numSteps)
       {
        swap = axisArray[index+1];
        axisArray[index+1] = axisArray[index];
        axisArray[index] = swap;
        swapped = true;
       }
      }     
  }
  while (swapped == true);
}
//**************************************************************************************************
void Move(int LX, int LY, int RX, int RY, float rpm)    //Moves all four axes the specified number of 
{                                                       //steps, relative to the current position. 
  int index;                                            //Negative values move in the negative angle 
  int axis2int = 0;                                     //direction.
  int axis3int = 0;
  int axis4int = 0;
  int lastAxis2int = 0;
  int lastAxis3int = 0;
  int lastAxis4int = 0;
  float rpmFactor;

  AssignMotorPins();


  //Speed is measured in RPM, and is controlled by the delay between steps
  rpmFactor = (20.0/rpm) * 366;                         


  //Stuff axisArray. Write high or low to the dir pin of each axis depending on the sign of the number of steps.
  axisArray[1].numSteps = abs(LX);
  if (LX < 0) {digitalWrite(_LX1dirPin,HIGH);} else {digitalWrite(_LX1dirPin,LOW);}
  
  axisArray[2].numSteps = abs(LY);
  if (LY < 0) {digitalWrite(_LY2dirPin,LOW);} else {digitalWrite(_LY2dirPin,HIGH);}         //Motor 2's orientation is opposite the others
  
  axisArray[3].numSteps = abs(RX);
  if (RX < 0) {digitalWrite(_RX3dirPin,HIGH);} else {digitalWrite(_RX3dirPin,LOW);}
  
  axisArray[4].numSteps = abs(RY);
  if (RY < 0) {digitalWrite(_RY4dirPin,HIGH);} else {digitalWrite(_RY4dirPin,LOW);}

  SortAxisArray();                                                  //Sort by .numSteps. Largest .numSteps will be element [1].
  
  GetSlopes();                                                      //Get slope of of axes [2..4] by dividing .numSteps by .numSteps of axis 1 
                                                                    //(axis 1 has largest number of steps)

                                                                    //Move the motors
  for (index = 1;index <= axisArray[1].numSteps;index++)            //Loop the number of steps of the axis with the most
  {                                                                 //steps, which has been sorted to axisArray[1].

    axis2int = index * axisArray[2].slope;                          //when index * an axis' slope increases by an integer
    axis3int = index * axisArray[3].slope;                          //amount, axis(n)int will increase.
    axis4int = index * axisArray[4].slope;
    
    digitalWrite(axisArray[1].stepPin,HIGH);                                       //axisArray[1] steps with every iteration
    
    if (axis2int > lastAxis2int) {digitalWrite(axisArray[2].stepPin,HIGH);}       //If axis(n)int increased, step that axis one step.
    if (axis3int > lastAxis3int) {digitalWrite(axisArray[3].stepPin,HIGH);}
    if (axis4int > lastAxis4int) {digitalWrite(axisArray[4].stepPin,HIGH);}
    delayMicrosecondsFlex(rpmFactor);    
    digitalWrite(axisArray[1].stepPin,LOW);
    if (axis2int > lastAxis2int) {digitalWrite(axisArray[2].stepPin,LOW);}  
    if (axis3int > lastAxis3int) {digitalWrite(axisArray[3].stepPin,LOW);}
    if (axis4int > lastAxis4int) {digitalWrite(axisArray[4].stepPin,LOW);}
    delayMicrosecondsFlex(rpmFactor); 


    lastAxis2int = axis2int;
    lastAxis3int = axis3int;
    lastAxis4int = axis4int;
  }

  
  currentLX = currentLX + LX;     //Update currentNN after the move
  currentLY = currentLY + LY;
  currentRX = currentRX + RX;
  currentRY = currentRY + RY;

}
//**************************************************************************************************************
void MoveTo(int targetLX, int targetLY, int targetRX, int targetRY, float rpm)    //Move to an absolute position,
{                                                                                 //rather than a position relative
                                                                                  //to the current one
  Move(targetLX-currentLX,targetLY-currentLY,targetRX-currentRX,targetRY-currentRY,rpm);

  currentLX = targetLX;
  currentLY = targetLY;
  currentRX = targetRX;
  currentRY = targetRY;
}
//**************************************************************************************************************
void MakeRadialPt(int deg,int radius)    //Returns x and y distances (in steps) for a point at a given angle and 
{                                        //radius relative to the current position. Results returned via the
  float vectorXFloat;                    //globals vectorX and vectorY.
  float vectorYFloat;
  float radian;

  radian = deg * (3.14159/180);

  vectorYFloat = sin(radian)*radius;
  vectorY = vectorYFloat;

  vectorXFloat = cos (radian)*radius;
  vectorX = vectorXFloat;
}
//**************************************************************************************************************
void Circle(int radius, float rpm)              //Move both eyes in a circle. rpm refers to the speed the 
{                                               //individual motors are turning, not the speed at which
  int index;                                    //the circle is traced

  for (index = 0;index <= 360;index++) 
   {
     MakeRadialPt(index,radius);
     MoveTo(vectorX,vectorY,vectorX,vectorY,rpm);
   }  
}
//**************************************************************************************************************
void RandomPointOnCircle(int centerX,int centerY,int innerRadius,int outerRadius,float rpm, bool eye)
                                                //Moves to a random point in a ring bounded by innerRadius and 
{                                               //outerRadius, at center point centerX, centerY. eye can be 
  int x,y,angle,radius;                         //_leftEye or _rightEye

  radius = random(innerRadius,outerRadius);
  angle = random(0,360);
  
  MakeRadialPt(angle,radius);

  if (eye =  _leftEye)
  {
    MoveTo(centerX+vectorX,centerY+vectorY,0,0,rpm);
  }
  else
  {
    MoveTo(0,0,centerX+vectorX,centerY+vectorY,rpm);
  }
}
//**************************************************************************************************************
void RandomlyCircleCenterPoint(int x,int y,int innerRadius,int outerRadius,int isi,float rpm, bool eye)
                                                  //Repeatedly calls RandomPointOnCircle at intervals of isi.
{
  Serial.write("Turn off stepper power to stop");   

  while (true)
  {
    RandomPointOnCircle(x,y,innerRadius,outerRadius,eye,rpm);

    delay(isi);
  }
}
//**************************************************************************************************************
void SetToZero()                                    //sets all current nn to 0
{
  currentLX = 0;
  currentLY = 0;
  currentRX = 0;
  currentRY = 0;
  Serial.println();Serial.println("Present position = 0,0,0,0");Serial.println();
}
//**************************************************************************************************************
void MoveToStart(float rpm)                         //Moves to the position defined as 0,0,0,0
{
  Move((currentLX*(-1)),(currentLY*(-1)),(currentRX*(-1)),(currentRY*(-1)),rpm);
}
//**************************************************************************************************************
void StuffTargetPoints(int targetPointArrayIndex)             //Called by SetTargetPoints to load the current 
{                                                             //eye positions into targetPointArray
    targetPointArray[targetPointArrayIndex].LX = currentLX; 
    targetPointArray[targetPointArrayIndex].LY = currentLY; 
    targetPointArray[targetPointArrayIndex].RX = currentRX; 
    targetPointArray[targetPointArrayIndex].RY = currentRY;
}
//**************************************************************************************************************
void SaveTargetPoints()                      //This is a simplistic way to preserve target points created with
{                                            //SetTargetPoints after the Arduino has been powered off. It 
   int index;                                //generates C assignment statements which can be retrieved from
                                             //the screen logfile and pasted (hard coded) into LoadSavedTargets
   ClearScreen();                            //for future use. Called at the conclusion of SetTargetPoints.
   Serial.println("Saving target points...");
   delay (1250);

   Serial.println();
   Serial.println();
   Serial.println();
   Serial.println();
   Serial.print("numTargetPoints = "); Serial.print(numTargetPoints); Serial.println(";");
   for (index = 0;index <= numTargetPoints;index++)  
   {
    Serial.print("targetPointArray["); Serial.print(index); Serial.print("].LX = ");Serial.print(targetPointArray[index].LX); Serial.println(";");
    Serial.print("targetPointArray["); Serial.print(index); Serial.print("].LY = ");Serial.print(targetPointArray[index].LY); Serial.println(";");
    Serial.print("targetPointArray["); Serial.print(index); Serial.print("].RX = ");Serial.print(targetPointArray[index].RX); Serial.println(";");
    Serial.print("targetPointArray["); Serial.print(index); Serial.print("].RY = ");Serial.print(targetPointArray[index].RY); Serial.println(";");
   }
   
   Serial.println();
   Serial.println();
   Serial.println();
   Serial.println();
}
//**************************************************************************************************************
void SetTargetPoints()                      //Creates a sequence of movements by manually positioning the eyes (via the keyboard) 
{                                           //to desired target positions and loading the points into targetPointArray. Saves the 
  int index = 1;                            //target point assignment statements to the screen logfile upon exit.

  ClearScreen();

  Serial.println("Position the eyes to the start point, 0,0,0,0:");
  Position();
  SetToZero();
  StuffTargetPoints(0);
  ClearScreen();

  Serial.print("Position the eyes to target 1.");
  Position();
  StuffTargetPoints(index);
  index++;

  while (globalChar != 'x')
  {
   globalChar = '-';
   ClearScreen();
   Serial.print("Press 't' to set target "); Serial.println(index);
   Serial.println("Press 'x' to stop setting points");
   globalChar=GetKey();
   if (globalChar == 't')
   {

     ClearScreen();
    
     MoveTo(0,0,0,0,5);
     Serial.println("*************************");
     Serial.println("SET ZERO POINT:");
     Serial.println("*************************");
     Position();
     SetToZero();

     ClearScreen();

     Serial.print("Move to target "); Serial.println(index);
     Position();
    
     StuffTargetPoints(index);
     index++;
   }
   
  }

  numTargetPoints = index-1;

  SaveTargetPoints();      //writes assignment statement to the output file

  delay(1000);

  ClearScreen();

  Serial.println();
}
//**************************************************************************************************************
void GoToTargetPoint(int targetPointArrayIndex,float rpm)       //Moves to a target point in targetPointArray
{  
  MoveTo(targetPointArray[targetPointArrayIndex].LX,targetPointArray[targetPointArrayIndex].LY,
  targetPointArray[targetPointArrayIndex].RX,targetPointArray[targetPointArrayIndex].RY,rpm);
}
//**************************************************************************************************************
void MoveThroughPoints()                                        //Moves through the points in targetPointArray.
{                                                               //After each cycle, pauses and allows the origin
 float rpm = 8;                                                 //to be adjusted if drift has occurred
 int index;
 int iterations;

 globalChar = "-";

 ClearScreen();
 Serial.println();
 Serial.println("Move through points");

 do
 {
   Serial.print("interation ");Serial.println(iterations);
   Serial.println();
   for (index = 0;index <= numTargetPoints;index++)  
   {
     
     GoToTargetPoint(index,rpm);

     delay(1000);
   }
   
    ClearScreen();

    MoveTo(0,0,0,0,5);
    Serial.println("Adjust to 0,0,0,0");
    Serial.println();
    Position();
    SetToZero();
    
    Serial.println("Type 'x' to stop moving, any other key to continue");
    globalChar=GetKey();
   
 } 
 while (globalChar != 'x');
 
}
//**************************************************************************************************************
void AdjustPoints()                       //Moves through the points in targetPointArray, pausing at each point
{                                         //and allowing adjustments to be made. Compensates for the effect of
 float rpm = 8;                           //inertia not present when slowly defining points one step at a time.
 int index;                               //Accuracy may be improved by running this function multiple times.
                                          //Saves the adjusted target point assignment statements to the screen
   ClearScreen();                         //logfile upon exit.
   Serial.println();                                    
   Serial.println("Adjust target points");
   Serial.println();
   Serial.println("Exit positioning (e) to move to the next point");

   for (index = 0;index <= numTargetPoints;index++)  
   {

   ClearScreen();
   Serial.println();
   Serial.println("Adjust target points");
   Serial.println();
   Serial.println("Exit positioning (e) to move to the next point");
   
    
    GoToTargetPoint(index,rpm);
  
    Position();

    targetPointArray[index].LX = currentLX;
    targetPointArray[index].LY = currentLY;
    targetPointArray[index].RX = currentRX;
    targetPointArray[index].RY = currentRY;
    
   }

   SaveTargetPoints();

}
//**************************************************************************************************************
void MovementTask()                                   //Insert a movement task here to be called by entering 't' in 
{                                                     //main menu
  
  while(true)
  {
    Circle(160,2);
  }
   
}
//**************************************************************************************************************
void LoadSavedTargets()                               //Paste target point assignment statements previously 
{                                                     //saved to the screen log file here. From the main menu,
                                                      //enter 'l' to load the targets into targetPointArray
numTargetPoints = 3;
targetPointArray[0].LX = 0;
targetPointArray[0].LY = 0;
targetPointArray[0].RX = 0;
targetPointArray[0].RY = 0;
targetPointArray[1].LX = -150;
targetPointArray[1].LY = 44;
targetPointArray[1].RX = -152;
targetPointArray[1].RY = 32;
targetPointArray[2].LX = 0;
targetPointArray[2].LY = 210;
targetPointArray[2].RX = -9;
targetPointArray[2].RY = 197;
targetPointArray[3].LX = 190;
targetPointArray[3].LY = 36;
targetPointArray[3].RX = 190;
targetPointArray[3].RY = 36;

}
//**************************************************************************************************************
void loop()                                           //Main menu
{  
    
  ClearScreen();

  Serial.println();
  Serial.println("Choose an option:");
  Serial.println();
  Serial.println(" p - position eyes");
  Serial.println(" s - set target points");
  Serial.println(" a - adjust target points");
  Serial.println(" m - move through target points");
  Serial.println(" t - start movement task");
  Serial.println(" z - initialize the present position to 0,0,0,0");
  Serial.println(" l - load saved targets");

  globalChar=GetKey();
  if (globalChar == 'p') {ClearScreen(); Position();}
  if (globalChar == 's') {SetTargetPoints();}
  if (globalChar == 'a') {AdjustPoints();}
  if (globalChar == 'm') {MoveThroughPoints();}
  if (globalChar == 't') {MovementTask();}
  if (globalChar == 'z') {SetToZero();}
  if (globalChar == 'l') {LoadSavedTargets();}

  ClearScreen();

}
